# My scripts for Solus

# Use

`git clone git@gitlab.com:leonevo/scripts.git && cd scripts`

# Scripts

## Bitwarden

_To install password manager client, Bitwarden

`./install-bitwarden-on-solus.sh`   

## Molotov 

_To install Molotov to watch TV channels

`./install-molotov-on-solus.sh`    

## Stremio 

_To install Stremio for watching movies and series by streaming     

`./install-stremio-on-solus.sh`

## Energized Linux 

_To modify your /etc/hosts file by adding [Energized Linux](https://github.com/EnergizedProtection/Energized_Linux) blacklist to prevent ads, trackers...(different levels available)

`sudo ./set-energized.sh`

## Cozy

_To install Cozy Cloud client

`./install-cozy-on-solus.sh`

## Oh My ZSH !

_To install Oh My ZSH! sur Solus

`./install-oh-my-zsh-on-solus.sh`

## Tor Browser ES

_To install Tor Browser (Spanish version) on Solus

`./install-torbrowser-es-ES.sh`

## French Data Network DNS

_To force the use of the DNS of FDN (french association respecting privacy)

`sudo ./set-FDN-dns.sh`

## Gotop

_To install the top monitor written in Go language, gotop

`./install-gotop.sh`

## Powerline9K

_To install Powerline9k theme on your zsh shell

`./install-powerlevel9k-on-zsh.sh`

## VirtualBox

_To install VirtualBox on Solus

`./install-virtualbox-on-solus.sh`

## OpenNIC DNS

_This script will force the use nearest DNS from OpenNIC

`sudo ./set-opennic-dns.sh`

## Joplin

_To install the Notes taking software, joplin (supporting Nextcloud, Dropbox...)

`./install-joplin-on-solus.sh`

## Protonmail-Desktop

_To install the web interface for ProtonMail, ProtonMail Desktop

`./install-protonmail-desktop-on-solus.sh`

## Whalebird

_To install Whalebird (Mastodon client)

`./install-whalebird.sh`

## Kernel Unstable

_To install the kernel from Solus unstable repo (not recommended)

`./install-kernel-solus-unstable.sh`

## ProtonVPN-CLI

_To install protonVPN CLI software

`./install-protonvpn-cli.sh`

## Gnome Themes / Thèmes GNOME

_To install more GNOME Themes

`./install-gnome-themes.sh`

## Steam Themes / Thèmes Steam

_To install themes for steam

`./install-steam-themes.sh`

## Freetube

_To install Freetube, Youtube app respecting privacy

`./install-freetube-on-solus.sh`

## Stubby

_To install and set up stubby for DNS over TLS

`./set-stubby.sh`

