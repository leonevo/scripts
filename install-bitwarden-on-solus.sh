#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O bitwarden.AppImage https://github.com/bitwarden/clients/releases/download/desktop-v2023.12.1/Bitwarden-2023.12.1-x86_64.AppImage

chmod +x bitwarden.AppImage

./bitwarden.AppImage
