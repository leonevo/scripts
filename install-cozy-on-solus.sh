#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O cozy64.AppImage https://github.com/cozy-labs/cozy-desktop/releases/download/v3.40.0-beta.1/Cozy-Drive-3.40.0-beta.1-x86_64.AppImage

chmod +x cozy64.AppImage

./cozy64.AppImage
