#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

# just in case
rm freetube.AppImage

wget -O freetube.AppImage https://github.com/FreeTubeApp/FreeTube/releases/download/v0.19.1-beta/freetube_0.19.1_amd64.AppImage

chmod +x freetube.AppImage

./freetube.AppImage
