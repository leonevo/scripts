#!/bin/bash
#
cd /tmp

git clone https://github.com/vinceliuice/Lavender-theme.git

cd Lavender-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Mojave-gtk-theme.git

cd Mojave-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Sierra-gtk-theme.git

cd Sierra-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/matcha.git

cd matcha && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Qogir-theme.git

cd Qogir-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/vimix-gtk-themes.git

cd vimix-gtk-themes && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Canta-theme.git

cd Canta-theme && ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/ChromeOS-theme.git

cd ChromeOS-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/WhiteSur-gtk-theme.git

cd WhiteSur-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Fluent-gtk-theme.git

cd Fluent-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Graphite-gtk-theme.git

cd Graphite-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Jasper-gtk-theme.git

cd Jasper-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Layan-gtk-theme.git

cd Layan-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Orchis-theme.git

cd Orchis-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Lavanda-gtk-theme.git

cd Lavanda-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Colloid-gtk-theme.git

cd Colloid-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Magnetic-gtk-theme.git

cd Magnetic-gtk-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/vimix-gtk-themes.git

cd vimix-gtk-themes && sudo ./install.sh
