#!/bin/bash
#
cd /tmp

git clone https://github.com/vinceliuice/Tela-icon-theme.git

cd Tela-icon-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Tela-circle-icon-theme.git

cd Tela-circle-icon-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/WhiteSur-icon-theme.git

cd WhiteSur-icon-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Qogir-icon-theme.git

cd Qogir-icon-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Fluent-icon-theme.git

cd Fluent-icon-theme && sudo ./install.sh

cd /tmp

git clone https://github.com/vinceliuice/Colloid-icon-theme.git

cd Colloid-icon-theme && sudo ./install.sh
