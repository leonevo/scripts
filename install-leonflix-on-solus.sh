#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

# just in case
rm Leonflix.AppImage

wget -O Leonflix.AppImage https://leonflix.net/downloads/Leonflix.AppImage

chmod +x Leonflix.AppImage

./Leonflix.AppImage
