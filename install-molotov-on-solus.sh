#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O molotov.AppImage https://desktop-auto-upgrade.molotov.tv/linux/Molotov-4.5.1.AppImage

chmod +x molotov.AppImage

./molotov.AppImage
