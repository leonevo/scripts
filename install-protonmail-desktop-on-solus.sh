#!/bin/bash

mkdir -p ~/AppImages

cd ~/AppImages

wget -O protonmail-desktop.AppImage https://github.com/unofficial-protonmail-desktop/application/releases/download/v1.2.1/Unofficial-desktop-client-for-ProtonMail-1.2.1.AppImage

chmod +x protonmail-desktop.AppImage

./protonmail-desktop.AppImage
