#!/bin/bash

sudo eopkg -y it git dialog wget openvpn

cd /tmp

git clone "https://github.com/protonvpn/protonvpn-cli"

cd protonvpn-cli

sudo ./protonvpn-cli.sh --install
