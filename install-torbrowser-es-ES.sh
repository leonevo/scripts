#!/bin/bash
#

## check version here: https://www.torproject.org/projects/torbrowser.html.en

## removing previous version
rm -rf $HOME/tor-browser_es-ES

cd /tmp

wget -O tor-browser-es-ES.tar.xz https://dist.torproject.org/torbrowser/13.0.5/tor-browser-linux-x86_64-13.0.5.tar.xz

tar xvf tor-browser-es-ES.tar.xz 

mv tor-browser_es-ES  $HOME/

echo
echo
echo
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "check your HOME folder for a directory called tor-browser_es-ES"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo
