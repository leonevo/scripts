#!/bin/bash
#

# dependencies

sudo eopkg upgrade
sudo eopkg install -y gcc make autoconf binutils xorg-server-devel
sudo eopkg install -y linux-current-headers libelf-devel

# source : https://download.virtualbox.org/virtualbox/7.0.12/
sudo eopkg it -y -c system.devel
wget https://download.virtualbox.org/virtualbox/7.0.12/VirtualBox-7.0.12-159484-Linux_amd64.run -O /tmp/virtualbox.run
sudo sh /tmp/virtualbox.run
sudo /sbin/vboxconfig

