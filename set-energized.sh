#!/bin/bash
if [ "$UID" -ne "0" ] #User check
then
   echo -e "Use this script as root."
   exit
else

# https://github.com/EnergizedProtection/Energized_Linux

cd /tmp

wget https://raw.githubusercontent.com/EnergizedProtection/Energized_Linux/master/energized.sh 

chmod +x energized.sh

sudo ./energized.sh

fi
